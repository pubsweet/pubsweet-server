# Notice

Deprecated in favour of our monorepo: [https://gitlab.coko.foundation/pubsweet/pubsweet - server package](https://gitlab.coko.foundation/pubsweet/pubsweet/tree/master/packages/server)
